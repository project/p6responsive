<?php
/**
 * @file
 * Settings for mobile devices.
 */

$form['mobile'] = array(
  '#group' => 'p6responsive-settings',
  '#type' => 'fieldset',
  '#title' => t('Mobile'),
  '#collapsible' => TRUE,
);
$form['mobile']['toggle_meta_handheldfriendly'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable HandheldFriendly meta tag'),
  '#default_value' => p6responsive_get_setting('mobile_handheldfriendly'),
  '#description' => t('')
);
$form['mobile']['toggle_meta_mobileoptimized'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable MobileOptimized meta tag'),
  '#default_value' => p6responsive_get_setting('mobile_mobileoptimized'),
  '#description' => t('')
);
$form['mobile']['toggle_meta_viewport'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable viewport meta tag'),
  '#default_value' => p6responsive_get_setting('mobile_viewport'),
  '#description' => t('')
);
$form['mobile']['toggle_js_scalefix'] = array(
  '#type' => 'checkbox',
  '#title' => t('Scalefix for iOS devices'),
  '#default_value' => p6responsive_get_setting('mobile_scalefix'),
  '#description' => t('')
);
$form['mobile']['toggle_meta_ioswebapp'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable IOS web app meta tag and JavaScript'),
  '#default_value' => p6responsive_get_setting('mobile_ioswebapp'),
  '#description' => t('Enables the script preventing links from opening in mobile safari. https://gist.github.com/1042026')
);
$form['mobile']['toggle_meta_cleartype'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable Cleartype in IE Mobile devices'),
  '#default_value' => p6responsive_get_setting('mobile_cleartype'),
  '#description' => t('')
);
$form['mobile']['toggle_touch_icons'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable apple-touch-icons'),
  '#default_value' => p6responsive_get_setting('mobile_touchicons'),
  '#description' => t('')
);
