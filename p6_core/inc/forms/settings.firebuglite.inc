<?php
/**
 * @file
 * Firebug Lite settings.
 */

$form['firebuglite'] = array(
  '#group' => 'p6responsive-settings',
  '#type' => 'fieldset',
  '#title' => t('Firebug Lite'),
  '#collapsible' => TRUE,
);
$form['firebuglite']['toggle_firebuglite'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable Firebug Lite'),
  '#default_value' => p6responsive_get_setting('firebug_lite'),
  '#description' => t('Loads <a href="@firebuglite">Firebug Lite</a> for easier development', array('@firebuglite' => 'https://getfirebug.com/firebuglite'))
);
$form['firebuglite']['firebuglite_options'] = array(
  '#type' => 'fieldset',
  '#title' => t('Firebug Lite Options'),
  '#collapsible' => FALSE,
  '#collapsed' => FALSE,
  '#states' => array(
    'invisible' => array(
     ':input[name="toggle_firebuglite"]' => array('checked' => FALSE),
    ),
  ),
);
$form['firebuglite']['firebuglite_options']['saveCookies'] = array(
  '#type' => 'checkbox',
  '#title' => t('saveCookies'),
  '#default_value' => p6responsive_get_setting('firebug_lite_savecookies'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['startOpened'] = array(
  '#type' => 'checkbox',
  '#title' => t('startOpened'),
  '#default_value' => p6responsive_get_setting('firebug_lite_startopened'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['startInNewWindow'] = array(
  '#type' => 'checkbox',
  '#title' => t('startInNewWindow'),
  '#default_value' => p6responsive_get_setting('firebug_lite_startinnewwindow'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['showIconWhenHidden'] = array(
  '#type' => 'checkbox',
  '#title' => t('showIconWhenHidden'),
  '#default_value' => p6responsive_get_setting('firebug_lite_showiconwhenhidden'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['overrideConsole'] = array(
  '#type' => 'checkbox',
  '#title' => t('overrideConsole'),
  '#default_value' => p6responsive_get_setting('firebug_lite_overrideconsole'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['ignoreFirebugElements'] = array(
  '#type' => 'checkbox',
  '#title' => t('ignoreFirebugElements'),
  '#default_value' => p6responsive_get_setting('firebug_lite_ignorefirebugelements'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['disableXHRListener'] = array(
  '#type' => 'checkbox',
  '#title' => t('disableXHRListener'),
  '#default_value' => p6responsive_get_setting('firebug_lite_disablexhrlistener'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['disableWhenFirebugActive'] = array(
  '#type' => 'checkbox',
  '#title' => t('disableWhenFirebugActive'),
  '#default_value' => p6responsive_get_setting('firebug_lite_disablewhenfirebugactive'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['enableTrace'] = array(
  '#type' => 'checkbox',
  '#title' => t('enableTrace'),
  '#default_value' => p6responsive_get_setting('firebug_lite_enabletrace'),
  '#return_value' => 'true',
);
$form['firebuglite']['firebuglite_options']['enablePersistent'] = array(
  '#type' => 'checkbox',
  '#title' => t('enablePersistent'),
  '#default_value' => p6responsive_get_setting('firebug_lite_enablepersistent'),
  '#return_value' => 'true',
);


