<?php
/**
 * @file
 * Firebug Lite settings.
 */

$form['windowsize'] = array(
  '#group' => 'p6responsive-settings',
  '#type' => 'fieldset',
  '#title' => t('Window size'),
  '#collapsible' => TRUE,
);
$form['windowsize']['toggle_windowsize'] = array(
  '#type' => 'checkbox',
  '#title' => t('Display window size'),
  '#default_value' => p6responsive_get_setting('window_size'),
  '#description' => t('Displays the width and the height of the window in a box on the bottom right corner of the window.'),
);
