<?php
/**
 * @file
 * Polyfills.
 */

$form['polyfills'] = array(
  '#group' => 'p6responsive-settings',
  '#type' => 'fieldset',
  '#title' => t('Polyfills'),
  '#collapsible' => TRUE,
);
$form['polyfills']['toggle_modernizrjs'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable Modernizr'),
  '#default_value' => p6responsive_get_setting('polyfill_modernizr'),
  '#description' => t('')
);
$form['polyfills']['toggle_respondjs'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enable media query support for IE6-8'),
  '#default_value' => p6responsive_get_setting('polyfill_respond'),
  '#description' => t('')
);
