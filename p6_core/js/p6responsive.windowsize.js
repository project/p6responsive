/**
 * @file
 * Displays the width and the height of the window in a box on the bottom right
 * corner of the window.
 */
(function ($) {
  $(document).ready(function () {
    $('<div id="p6responsive-window-size">' + $(window).width() + 'x' + $(window).height() + '</div>')
      .attr('unselectable', 'on')
      .css({
        'position' : 'fixed',
        'bottom' : 0,
        'right' : 0,
        'background' : '#000000',
        'color' : '#FFFFFF',
        'font' : '16px/1 sans-serif',
        'padding' : '10px',
        'z-index' : 999999,
        'cursor' : 'default',
        '-moz-user-select' : 'none',
        '-webkit-user-select' : 'none',
        'user-select' : 'none',
        '-ms-user-select' : 'none'
      })
      .appendTo('body');
  });
  $(window).resize(function () {
    $("#p6responsive-window-size").text($(window).width() + 'x' + $(window).height());
  });
})(jQuery);
