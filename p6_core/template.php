<?php
/**
 * @file
 * Core theme template settings and configurations.
 */
global $theme_key, $p6responsive_path_core;
$theme_key = $GLOBALS['theme_key'];
$p6responsive_path_core = drupal_get_path('theme', 'p6responsive');


/**
 * Implements template_process().
 */
function p6responsive_preprocess(&$variables, $hook) {

}

/**
 * Implements template_preprocess_html().
 */
function p6responsive_preprocess_html(&$variables) {
  $variables['base_path'] = base_path();

  // Get settings.
  $variables['p6responsive_js_modernizr']  = theme_get_setting('p6responsive_js_modernizr');
  $variables['p6responsive_meta_viewport'] = theme_get_setting('p6responsive_meta_viewport');
//$variables['p6_meta_xua_compatible'] = theme_get_setting('p6_meta_xua_compatible');

  // META tag for
  // if (theme_get_setting('p6_meta_xua_compatible')) {
  //   // More info: h5bp.com/i/378.
  //   $p6_meta_xua_compatible = array(
  //     '#type' => 'html_tag',
  //     '#tag' => 'meta',
  //     '#attributes' => array(
  //       'http-equiv' => 'X-UA-Compatible',
  //       'content' => 'IE=edge,chrome=1',
  //     ),
  //     '#weight' => '-99999',
  //   );
  //   drupal_add_html_head($p6_meta_xua_compatible, 'p6_meta_xua_compatible');
  // }

  drupal_add_http_header('X-UA-Compatible', 'IE=edge,chrome=1');


  // Firebug Lite.
  drupal_add_js('https://getfirebug.com/firebug-lite.js#saveCookies=true,showIconWhenHidden=true', array(
    'type' => 'external',
    'scope' => 'header',
    'group' => JS_DEFAULT,
    'every_page' => TRUE,
    'weight' => -100,
  ));
}

/**
 * Implements template_preprocess_page().
 */
function p6responsive_preprocess_page(&$variables) {
  // if (!isset($variables['title'])) {
  //   $variables['title'] = drupal_get_title();
  // }

  // $variables['p6_header_nav_button'] = l(
  //   'Toggle',
  //   '',
  //   array(
  //     'fragment' => 'toggle',
  //     'external' => TRUE,
  //     'attributes' => array(
  //       'title' => 'Toggle content',
  //       'id' => 'toggle-navigation',
  //       'data-toggle' => 'collapse',
  //       'data-target' => 'navigation',
  //       'class' => array('p6-btn-toggle', 'element-invisible'),
  //     ),
  //   )
  // );

  // Theme logo linked to front page.
  // $variables['p6_site_logo'] = '';
  // $variables['p6_site_logo_attributes_array'] = array(
  //         'rel' => 'home',
  //         'id' => 'p6-site-logo',
  //         'title' => t('Home'),
  //         'class' => array('brand'),
  //       );
  // if ($variables['logo']) {
  //   $variables['p6_site_logo'] = l(
  //     theme(
  //       'image',
  //       array(
  //         'path' => $variables['logo'],
  //         'alt' => $variables['site_name'],
  //       )
  //     ),
  //     '<front>', // $variables['front_page'],
  //     array(
  //       'attributes' => $variables['p6_site_logo_attributes_array'],
  //       'html' => TRUE,
  //     )
  //   );
  // }

  // Site name and logo.
  // $variables['p6_site_name_and_slogan'] = '';
  // if ($variables['site_name'] || $variables['site_slogan']) {
  //   $variables['p6_site_name_and_slogan'] = '<hgroup id="p6-site-name-and-slogan" class="brand">' .
  //     (isset($variables['title']) ? '<h2 id="p6-site-name" class="navbar-text">' . $variables['site_name'] .'</h2>' : '<h1 id="p6-site-name" class="navbar-text">' . $variables['site_name'] .'</h1>') .
  //     (($variables['site_slogan']) ? '<h2 id="p6-site-slogan" class="navbar-text">' . $variables['site_slogan'] . '</h2>' : '') .
  //     '</hgroup>';
  // }

  // // Build a variable for the main menu
  // if (isset($variables['main_menu'])) {
  //   $variables['primary_navigation'] = theme('links', array(
  //     // 'links' => menu_main_menu(),
  //     'links' => menu_navigation_links('main-menu'),
  //     'attributes' => array(
  //       'class' => array('nav', 'clearfix'),
  //      ),
  //     'heading' => array(
  //       'text' => t('Main menu'),
  //       'level' => 'h2',
  //       'class' => array('element-invisible'),
  //     )
  //   ));
  // }

  // Site navigation links.
  $vars['main_menu_links'] = '';
  if (isset($vars['main_menu'])) {
    $vars['main_menu_links'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'id' => 'main-menu',
        'class' => array('inline', 'main-menu'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
  $vars['secondary_menu_links'] = '';
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_menu_links'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'id'    => 'secondary-menu',
        'class' => array('inline', 'secondary-menu'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }

  // Get main menu 1st and 2nd level.
  // If you need more levels - just change 2 below to any other number.
  // $main_menu = menu_tree_output(menu_tree_all_data(variable_get('menu_main_links_source', 'main-menu'), NULL, 2));

  // // Custom wrapper for 1st menu level.
  // $main_menu['#theme_wrappers'] = array('menu_tree__main_menu_primary');

  // $variables['page']['main_menu'] = $main_menu;

  // $variables['p6responsive_search'] = FALSE;
  // if (theme_get_setting('toggle_search') && module_exists('search')) {
  //   $variables['p6responsive_search'] = drupal_get_form('_p6responsive_search_form');
  // }


}

/**
 * Theme wrapper for 2nd (and deeper) level of main menu
 */
// function p6responsive_menu_tree__main_menu($variables) {
//   return  '<ul class="dropdown-menu">' . $variables['tree'] . '</ul>';
// }

/**
 * Theme wrapper for 1st level of main menu
 */
// function p6responsive_menu_tree__main_menu_primary($variables) {
//   return '<ul class="nav">' .  $variables['tree'] . '</ul>';
// }

// function p6responsive_menu_link(array $variables) {
//   $element = $variables['element'];
//   $sub_menu = '';

//   $caret = '';

//   if ($element['#below']) {
//     $sub_menu = drupal_render($element['#below']);
//     $element['#attributes']['class'] = array('dropdown');
//     $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
//     $element['#localized_options']['attributes']['data-target'] = '#';
//     $element['#localized_options']['attributes']['class'] = array('dropdown-toggle');
//     $element['#localized_options']['html'] = TRUE;

//     $caret = ' <b class="caret"></b>';
//   }
//   $output = l($element['#title'] . $caret, $element['#href'], $element['#localized_options']);
//   return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
// }

// function p6responsive_preprocess_region(&$variables) {
//   $region = $variables['region'];

//   // Add a "sidebar" class to sidebar regions
//   if (strpos($region, 'sidebar_') === 0) {
//     $variables['classes_array'][] = 'sidebar';
//     // Allow a region-specific template to override the region--sidebar suggestion.
//     array_unshift($variables['theme_hook_suggestions'], 'region__sidebar');
//   }
// }

// function p6responsive_process_region(&$variables) {
//   // Initialize and populate the outer wrapper variables
//   $variables['outer_prefix'] = '<div class="' . $variables['classes'] . '">';
//   $variables['outer_suffix'] = '</div>';

//   // Initialize and populate the inner wrapper variables
//   $variables['inner_prefix'] = '<div class="region-inner clearfix">';
//   $variables['inner_suffix'] = '</div>';

//   // Some regions need different or no markup
//   // Use a region template with no wrappers for the main content
//   if ($variables['region'] === 'content') {
//     $variables['outer_prefix'] = '';
//     $variables['outer_suffix'] = '';
//     $variables['inner_prefix'] = '';
//     $variables['inner_suffix'] = '';
//   }
//   // Menu bar needs an id, nav class and no inner wrapper
//   if ($variables['region'] === 'menu_bar') {
//     $variables['outer_prefix'] = '<div id="menu-bar" class="nav clearfix">';
//     $variables['inner_prefix'] = '';
//     $variables['inner_suffix'] = '';
//   }
// }

// function p6responsive_button($variables) {
//   $element = $variables['element'];
//   $element['#attributes']['type'] = 'submit';
//   element_set_attributes($element, array('id', 'name', 'value'));

//   $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
//   $element['#attributes']['class'][] = 'btn';
//   if (!empty($element['#attributes']['disabled'])) {
//     // $element['#attributes']['class'][] = 'form-button-disabled';
//     $element['#attributes']['class'][] = 'disabled';
//   }
//   // dsm($element);
//   return '<button' . drupal_attributes($element['#attributes']) . '>' . $element['#value'] . '</button>';
// }



// function p6responsive_menu_local_tasks(&$variables) {
//   $output = '';

//   if (!empty($variables['primary'])) {
//     $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
//     $variables['primary']['#prefix'] .= '<ul class="nav nav-tabs primary">';
//     $variables['primary']['#suffix'] = '</ul>';
//     $output .= drupal_render($variables['primary']);
//   }
//   if (!empty($variables['secondary'])) {
//     $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
//     $variables['secondary']['#prefix'] .= '<ul class="nav nav-tabs secondary">';
//     $variables['secondary']['#suffix'] = '</ul>';
//     $output .= drupal_render($variables['secondary']);
//   }

//   return $output;
// }


/**
 * Form builder; Output a search form for the search block's search box.
 *
 * @ingroup forms
 * @see search_box_form_submit()
 * @see search-block-form.tpl.php
 */
// function p6responsive_form_alter(&$form, &$form_state, $form_id) {
//   if ($form_id == 'search_block_form') {
//     $form[$form_id]['#attributes']['title'] = t('Heyooo');
//     $form[$form_id]['#attributes']['placeholder'] = t('Keywords');
//     $form[$form_id]['#attributes']['class'][] = 'span2';
//     // $form[$form_id]['#description'] = t('heyooooo');
//     // $form[$form_id]['#prefix'] = '<span class="add-on"><i class="icon-search"></i></span>';

//     $form['actions']['submit']['#value'] = t('!icon Search', array('!icon' => '<i class="icon-search"></i>'));
//     $form['#attributes']['class'][] = 'form-horizantal';
//   }
// }



