<?php

function p6responsive_get_setting($setting_name, $theme = NULL) {
  $cache = &drupal_static(__FUNCTION__, array());

  // If no key is given, use the current theme if we can determine it.
  if (!isset($theme)) {
    $theme = !empty($GLOBALS['theme_key']) ? $GLOBALS['theme_key'] : '';
  }

  if (empty($cache[$theme])) {
    // Get the values for the theme-specific settings from the .info files of
    // the theme and all its base themes.
    if ($theme) {
      $themes = list_themes();
      $theme_object = $themes[$theme];

      // Create a list which includes the current theme and all its base themes.
      if (isset($theme_object->base_themes)) {
        $theme_keys = array_keys($theme_object->base_themes);
        $theme_keys[] = $theme;
      }
      else {
        $theme_keys = array($theme);
      }
      foreach ($theme_keys as $theme_key) {
        if (!empty($themes[$theme_key]->info['settings'])) {
          $cache[$theme] = $themes[$theme_key]->info['settings'];
        }
      }

      // Get the saved global settings from the database.
      $cache[$theme] = array_merge($cache[$theme], variable_get('theme_settings', array()));
    }
  }

  return isset($cache[$theme][$setting_name]) ? $cache[$theme][$setting_name] : NULL;
}



/**
 * Implements hook_form_system_theme_settings_alter().
 */
function p6responsive_form_system_theme_settings_alter(&$form, &$form_state) {
  global $p6responsive_path_core;
  $p6responsive_path_core = drupal_get_path('theme', 'p6responsive');

  $form['p6responsive-settings'] = array(
    '#type' => 'vertical_tabs',
    // '#default_tab' => 'firebuglite',
    '#description' => t('General'),
    '#prefix' => '<div class="p6responsive-settings-form"><div class="clearfix"><h2>Settings</h2></div>',
    '#suffix' => '</div>',
    '#weight' => -10,
  );

  require_once($p6responsive_path_core . '/inc/forms/settings.mobile.inc');
  require_once($p6responsive_path_core . '/inc/forms/settings.polyfills.inc');
  require_once($p6responsive_path_core . '/inc/forms/settings.windowsize.inc');
  require_once($p6responsive_path_core . '/inc/forms/settings.firebuglite.inc');

  // Collapse annoying forms
  $form['theme_settings']['#collapsible'] = TRUE;
  $form['theme_settings']['#collapsed'] = TRUE;
  $form['logo']['#collapsible'] = TRUE;
  $form['logo']['#collapsed'] = TRUE;
  $form['favicon']['#collapsible'] = TRUE;
  $form['favicon']['#collapsed'] = TRUE;

  // dsm($form_state);
}
