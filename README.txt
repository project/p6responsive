Development Tools
-----------------
[ ] Enable Firebug Lite

    Firebug Lite options:
    [ ] saveCookies - false
    [ ] startOpened - false
    [ ] startInNewWindow - false
    [ ] showIconWhenHidden - true
    [ ] overrideConsole - true
    [ ] ignoreFirebugElements - true
    [ ] disableXHRListener - false
    [ ] disableWhenFirebugActive - true
    [ ] enableTrace - false
    [ ] enablePersistent - false


[ ] Display window size - appears in the bottom right corner

[ ] Wrap menu item text in SPAN tags - useful for certain theme or design related techniques

[ ] Remove the frontpage title

[ ] Do not display the Main content block on the front page

[ ] Remove RSS feed icons

Polyfills
=========
[ ] HTML5 support in IE
    By checking this setting the site will load the html5shiv. Turning this off will be bad news for IE6-8.

[ ] Media query support for IE6-8
    By checking this setting IE6, 7 and 8 will rely on respond.js to set the layout.

[ ] Selectivizr is a JavaScript utility that emulates CSS3 pseudo-classes and attribute selectors in Internet Explorer 6-8.

[ ] Scalefix for iOS

Mobile
======
[ ] Meta HandheldFriendly

[ ] Meta MobileOptimized

[ ] Meta viewport

[ ] Meta iOS web app (apple-mobile-web-app-capable)

[ ] Enable the script preventing links from opening in mobile safari. https://gist.github.com/1042026

[ ] Enable Chrome Edge support for IE

[ ] Enable Cleartype in IE Mobile devices


[ ] Enable Modernizr

[ ] Enable Firebug Lite

[ ] Change file icons http://goo.gl/JEiDj

